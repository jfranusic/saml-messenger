#!/usr/bin/env python
import argparse
import base64
import xml.dom.minidom
import sys
import urlparse
import urllib
from xml.sax.saxutils import escape as xml_escape

from requests import Request
import requests

from processform import ProcessForm
from models import HTTPRequest
from models import HTTPResponse


class SAMLValidator:
    def status_kind(self, value):
        if value:
            return 'OK'
        else:
            return 'FAIL'

    def response_values(self, resp, force_status=False):
        if 200 <= resp.status_code < 400:
            request_successful = True
        else:
            request_successful = False
        values = {
            'method': resp.request.method,
            'url': resp.request.url,
            'code': resp.status_code,
            'reason': resp.reason,
            'status': self.status_kind(request_successful),
        }
        summary = "{method} {url} {code} {reason}".format(**values)
        rv = {
            'kind': 'response',
            'summary': summary,
            'status': self.status_kind(request_successful),
            }
        if force_status:
            rv['status'] = force_status
        rv['status'] = 'INFO'
        # return rv
        # if not self.debug:
        #     return
        request = HTTPRequest(resp.request)
        response = HTTPResponse(resp)
        details = "{}\n{}\n\n{}\n{}\n".format(
            str(request.headers),
            str(request.body),
            str(response.headers),
            str(response.body),
            )
        # print("DETAILS: {}".format(details))
        rv['details'] = xml_escape(details)
        return rv

    def print_status(self, msg, data=False, status=None):
        rv = {
            'kind': 'status',
            'summary': msg,
            }
        if status:
            rv['status'] = status
        if self.debug and data:
            rv['details'] = xml_escape(data)
        self.responses.append(rv)

    def decode_saml_response(self, saml_response):
        try:
            decoded = base64.b64decode(saml_response)
            rv = xml.dom.minidom.parseString(decoded).toprettyxml()
        except:
            rv = 'Unable to decode SAMLResponse'
        return(rv)

    def decode_saml_request(self, saml_request):
        import urllib2
        req_unquoted = urllib2.unquote(saml_request)
        req_compressed = base64.b64decode(req_unquoted)
        import zlib
        req_uncompressed = zlib.decompress(req_compressed, -15)
        rv = xml.dom.minidom.parseString(req_uncompressed).toprettyxml()
        return rv

    def run_request(self, req, session):
        # FIXME: Why do I need to prepare a request?
        prepped = session.prepare_request(req)
        resp = session.send(prepped)
        for past_resp in resp.history:
            self.responses.append(self.response_values(past_resp))
            url = past_resp.url
            if 'SAMLRequest' not in url:
                continue
            parsed_url = urlparse.urlparse(url)
            parts = urlparse.parse_qs(parsed_url.query)
            saml_request = self.decode_saml_request(parts['SAMLRequest'][0])
            self.print_status('Decoded SAMLRequest', saml_request)
        self.responses.append(self.response_values(resp))
        return resp

    # Remove NotAfter
    def get_saml_response(self, req, s):
        while True:
            # --- Send a response ---
            resp = self.run_request(req, s)
            form = ProcessForm(resp.text, resp.request.url)
            if not form.is_form:
                break
            payload = form.payload
            # FIXME
            try:
                for k in self.form_fields.keys():
                    v = self.form_fields[k]
                    if k in payload:
                        payload[k] = v
            except:
                pass
            extra_msg = 'Found form to "{method}" to "{action}"'.format(
                method=(form.method).upper(),
                action=form.action,
                )
            self.print_status(extra_msg, str(form))
            # Since we found a form, set up the next request
            req = Request(form.method, form.action, data=form.payload)
            # If there is a SAMLResponse in the form,
            # break out of this loop and connect to the SP
            if 'SAMLResponse' in form.payload:
                self.found_saml_response = True
                saml = self.decode_saml_response(form.payload['SAMLResponse'])
                self.print_status('Decoded SAMLResponse', saml)
                break
        return req

    def __init__(self, url=None, form_fields=None, debug=None):
        self.responses = []
        self.idp_url = url
        self.form_fields = form_fields
        self.debug = debug
        self.test_security = False
        self.max_redirects = 5

        self.session = requests.Session()
        self.session.max_redirects = self.max_redirects

    def post_updates_run(self, data):
        first_request = Request('POST', self.idp_url, data=data)
        req = self.get_saml_response(first_request, self.session)
        s = self.session
        self.found_saml_response = 'SAMLResponse' in req.data
        if self.found_saml_response:
            # self.print_status('Sending request to SP')
            self.run_request(req, s)
        else:
            self.print_status('No SAMLResponse Found!', status=False)
        if self.found_saml_response and self.test_security:
            # print("found SAML: {} test security: {}".format(
            #     self.found_saml_response,
            #     self.test_security))
            from request_manglers import mangler_list
            for mangler_class in mangler_list:
                mangle_session = requests.Session()
                mangle_session.max_redirects = self.max_redirects
                responses_setup_start = len(self.responses)
                req = self.get_saml_response(first_request, mangle_session)
                mangler = mangler_class(req.data['SAMLResponse'])
                req.data['SAMLResponse'] = mangler.get()
                saml = self.decode_saml_response(req.data['SAMLResponse'])
                setup_responses = []
                if len(self.responses) > responses_setup_start:
                    setup_responses = self.responses[responses_setup_start:]
                    del(self.responses[responses_setup_start:])
                summary = "Fetched a fresh SAML Response to modify"
                details = ''
                for r in setup_responses:
                    if 'summary' in r:
                        details += r['summary'] + "\n\n"
                    if 'details' in r:
                        details += r['details'] + "\n"
                update = {
                    'kind': 'security',
                    'summary': summary,
                    'details': details
                }
                self.responses.append(update)
                docstring = mangler.__doc__
                msg = "Created SAMLResponse that {}".format(docstring)
                self.print_status(msg, saml)

                resp = requests.request(
                    req.method,
                    req.url,
                    data=req.data,
                    )
                (status_message, status) = mangler.success(resp)
                status_type = self.status_kind(status)
                details = ''
                for past_resp in resp.history:
                    r = self.response_values(past_resp, 'info')
                    if 'summary' in r:
                        details += r['summary'] + "\n\n"
                    if 'details' in r:
                        details += r['details'] + "\n"
                r = self.response_values(resp, 'info')
                if 'summary' in r:
                    details += r['summary'] + "\n\n"
                if 'details' in r:
                    details += r['details'] + "\n"
                update = {
                    'kind': 'security',
                    'summary': status_message,
                    'status': status_type,
                    'details': details,
                }
                self.responses.append(update)

    def post_updates(self, data):
        try:
            self.post_updates_run(data)
        except Exception as e:
            rv = {
                'status': 'FAIL',
                'summary': str(e)
            }
            self.responses.append(rv)
        for response in self.responses:
            yield response


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser(
        description='SAML Response tool')
    arg_parser.add_argument('--idp',
                            dest='idp_url',
                            help='URL for the IdP',
                            required=True)
    arg_parser.add_argument('--debug',
                            dest='debug',
                            action='store_true',
                            help='Enable debugging output')
    arg_parser.add_argument('--mangle',
                            dest='mangle',
                            action='store_true',
                            help='Mangle SAMLResponse')
    arg_parser.add_argument('--username',
                            dest='username',
                            help='Username to use in login forms')
    arg_parser.add_argument('--password',
                            dest='password',
                            help='Password touse in login form')
    arg_parser.add_argument('--input',
                            dest='input',
                            action='append',
                            help='key=value')
    args = arg_parser.parse_args()

    url = args.idp_url
    form_fields = {}
    debug = args.debug

    # FIXME: Make this parse out the k=v values from "input"
    # if args.input:
    #     # (k, v) = input_value.split('=')
    #     form_fields = args.input
    if args.username:
        form_fields['username'] = args.username
    if args.password:
        form_fields['password'] = args.password

    validator = SAMLValidator(url=url, form_fields=form_fields, debug=debug)
    if args.mangle:
        validator.test_security = True

    for update in validator.get_updates():
        print "-" * 5
        pprint.pprint(update)
        print "-" * 5
