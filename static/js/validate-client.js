var what = '';

function validateSAML(url, input) {
    $('#accordion').accordion();
    $('#results').empty();
    input.url = url;
    var spinner = '<img id="spinner" src="static/img/spin.gif">';
    $('#results').append(spinner);
    $.ajax({
	dataType: "json",
	type: 'POST',
	url: "api/v1/session",
	data: input,
    }).done(function( data ) {
	what = data;
	$('#spinner').remove();
	$('#results').append('<div id="accordion"></div>');
	$.each( data.results, function( x ) {
	    var result = data.results[x];
	    var result_kind = "result-kind-" + result.kind;
	    var result_status = '';
	    // var result_status = "result-status-" + result.status;
	    var result_details = '';
	    if ( result.details ) {
		result_details = "<pre>" + result.details + "</pre>";
	    }
	    var status_type = 'info';
	    if ( result.status == 'OK' ) {
		status_type = 'ok';
	    } else if ( result.status == 'FAIL' ) {
		status_type = 'exclamation';
	    }
	    var status_tag =  '<span ' + 
		'class="glyphicon glyphicon-' + status_type + '-sign" ' + 
		'aria-hidden="true"></span> ';

	    var contents = "<h3 " + 
		// "class='result " + 
		// result_kind + " " +  result_status + "'" +
		">" + 
		status_tag + 
		// '<span class="sr-only">Error:</span>' +
		result.summary + 
		'</h3>' +
		'<div>' + 
		result_details +
		"</div>";
	    $('#accordion').append(contents);
	});
	$( "#accordion" ).accordion({
	    heightStyle: "content",
	    active: false,
	    collapsible: true
	});
    });
}

function submitForm ( form_fields ) {
}

$('form').submit(function(event) {
    var url = $('#input-saml-acs').val();
    var extended_validation = $('#input-extended-validation').is(':checked');
    var form_fields = {
        issuer: $('#input-saml-issuer').val(),
        acs: $('#input-saml-acs').val(),
        audience: $('#input-saml-aud').val(),
        username:  $('#input-username').val(),
        firstName: $('#input-first-name').val(),
        lastName:  $('#input-last-name').val(),
	test_security: extended_validation
    };
    validateSAML(url, form_fields);
    event.preventDefault();
});

function showHideSamlAttributes() {
    if($('#saml-attributes div').is(':visible')) {
	$('#saml-attributes').hide();
	$('#show-saml-attributes a span').text('Show');
    } else {
	$('#saml-attributes').show();
	$('#show-saml-attributes a span').text('Hide');
    }
}

$( document ).ready(function() {
    showHideSamlAttributes();
    $('#show-saml-attributes a').click(function() {
	showHideSamlAttributes();
    });
    // var url = 'http://jf-example-saml-sp.azurewebsites.net/saml/acs/testing';
    // var form_fields = {
    //     acs: 'http://jf-example-saml-sp.azurewebsites.net/saml/acs/testing',
    //     audience: 'http://jf-example-saml-sp.azurewebsites.net/saml/acs/okta',
    // 	username: 'testing@example.com',
    // 	firstName: 'testFirst',
    // 	lastName: 'testLast',
    // 	test_security: 'true'
    // 	// test_security: 'false'
    // };
    // validateSAML(url, form_fields);
});
