#!/usr/bin/env python
from bs4 import BeautifulSoup
import urlparse


class ProcessForm:
    def __init__(self, html=False, default_url='http://httpbin.org/post'):
        self.is_form = False
        self.action = default_url
        self.method = None
        self.payload = {}

        if html:
            soup = BeautifulSoup(html)
        else:
            return

        try:
            action = soup.form['action']
        except:
            action = ''

        if action.startswith('http'):
            self.action = action
        else:
            parsed_url = urlparse.urlparse(default_url)
            new_url = "{scheme}://{netloc}/{path}".format(
                scheme=parsed_url.scheme,
                netloc=parsed_url.netloc,
                path=action)
            self.action = new_url

        try:
            self.method = soup.form['method']
        except:
            self.method = 'POST'

        try:
            inputs = soup.form.find_all('input')
            self.is_form = True
        except:
            inputs = []

        for tag in inputs:
            name = None
            value = None
            try:
                name = tag['name']
            except:
                pass
            try:
                value = tag['value']
            except:
                pass

            self.payload[name] = ''
            if value:
                self.payload[name] = value

    def __str__(self):
        rv = ""
        rv += "Form 'action': {action}\nForm 'method': {method}\n".format(
            action=self.action,
            method=self.method)
        for key in self.payload.keys():
            rv += "    {key}: {value}\n".format(
                key=key,
                value=self.payload[key])
        return rv
