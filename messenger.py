#!/usr/bin/env python
import argparse
import base64
import xml.dom.minidom
import sys
import urlparse
import urllib

from requests import Request
from termcolor import colored
import requests

from processform import ProcessForm
from models import HTTPRequest
from models import HTTPResponse


arg_parser = argparse.ArgumentParser(
    description='SAML Response tool')
arg_parser.add_argument('--idp',
                        dest='idp_url',
                        help='URL for the IdP',
                        required=True)
arg_parser.add_argument('--debug',
                        dest='debug',
                        action='store_true',
                        help='Enable debugging output')
arg_parser.add_argument('--mangle',
                        dest='mangle',
                        action='store_true',
                        help='Mangle SAMLResponse')
arg_parser.add_argument('--username',
                        dest='username',
                        help='Username to use in login forms')
arg_parser.add_argument('--password',
                        dest='password',
                        help='Password touse in login form')
arg_parser.add_argument('--input',
                        dest='input',
                        action='append',
                        help='key=value')
args = arg_parser.parse_args()


'''
= Command Line Options
*The point is to get a SAML Response*

--idp=https://idp.example.com/login
--username=user@example.com
--password=abc123
--sp=https://application.sp.example.com/login
--idp-link=http://idp.example.com/application

= Use Cases
IdP Initiated
Link Initiated
SP Initiated

Logged in
Not logged in

IdP directs to SP
IdP dangling, doesn't direct to SP

= Flows
PoC  [Good] IdP, logged into IdP, directs to SP (saml-idp)
           IdP > IdP (SAMLResponse) > SP
TODO [Good] IdP, logged into IdP, dangling (Okta)
           IdP > IdP Page
TODO [Good] IdP, logged out, directs to SP (???)
           IdP > IdP Login Page > IdP > IdP (SAMLResponse) > SP
TODO [Good] IdP, logged out, dangling (Okta)
           IdP > IdP Login Page > IdP Page

TODO [Good] Link, logged into IdP, directs to SP (Okta)
           IdP Link > IdP > IdP (SAMLResponse) > SP
TODO [Bad]  Link, logged into IdP, dangling
           IdP Link > IdP > IdP (SAMLResponse) > IdP PageT
ODO [Good] Link, logged out, directs to SP (Okta)
           IdP Link > IdP Login Page > IdP > IdP (SAMLResponse) > SP
TODO [Bad]  Link, logged out, dangling
           IdP Link > IdP Login Page > IdP > IdP (SAMLResponse) > IdP Page

TODO [Good] SP, logged into IdP, directs to SP (Okta)
           SP > IdP > IdP (SAMLResponse) > SP
TODO [Bad]  SP, logged into IdP, dangling
           SP > IdP > IdP (SAMLResponse) > IdP Page
TODO [Good] SP, logged into SP, directs to SP (All)
           SP
TODO [Good] SP, logged out, directs to SP (Okta)
           SP > IdP > IdP Login Page > IdP > IdP (SAMLResponse) > SP
TODO [Bad]  SP, logged out, dangling
           SP > IdP > IdP Login Page > IdP > IdP Page


= SAMLResponse Tests

  * Remove XML signature
  * Change Issuer
  * Add assertion outside of XML signature
  * XML extension
  * Try to exploit this: https://github.com/onelogin/python-saml/blob/6cfa04c7192d1a23fe1006846c0e639d94042676/onelogin/saml/SignatureVerifier.py#L86

= Summary mode

[Ok]   IdP, Login, SP
[FAIL] IdP, Not Logged in, SP

'''


def status_kind(value):
    if value:
        return colored(' OK ', 'green')
    else:
        return colored('FAIL', 'red')


def print_response(resp):
    if 200 <= resp.status_code < 400:
        request_successful = True
    else:
        request_successful = False
    values = {
        'method': resp.request.method,
        'url': resp.request.url,
        'code': resp.status_code,
        'reason': resp.reason,
        'status': status_kind(request_successful),
    }
    print("[{status}] {method} {url} {code} {reason}".format(**values))
    if not args.debug:
        return
    request = HTTPRequest(resp.request)
    response = HTTPResponse(resp)
    print(colored(request.headers, 'red'))
    print(colored(request.body, 'blue'))
    print("")
    print(colored(response.headers, 'green'))
    print(colored(response.body, 'blue'))
    print("\n")


def print_status(msg, data=False, status=None):
    if status is None:
        summary = '      '
    else:
        summary = "[{kind}]".format(kind=status_kind(status))
    print("{summary} {msg}".format(summary=summary, msg=msg))
    if args.debug and data:
        print colored(data, 'yellow')


def decode_saml_response(saml_response):
    try:
        decoded = base64.b64decode(saml_response)
        rv = xml.dom.minidom.parseString(decoded).toprettyxml()
    except:
        rv = 'Unable to decode SAMLResponse'
    return(rv)


def decode_saml_request(saml_request):
    import urllib2
    req_unquoted = urllib2.unquote(saml_request)
    req_compressed = base64.b64decode(req_unquoted)
    import zlib
    req_uncompressed = zlib.decompress(req_compressed, -15)
    rv = xml.dom.minidom.parseString(req_uncompressed).toprettyxml()
    return rv


def run_request(req, session):
    # FIXME: Why do I need to prepare a request?
    prepped = session.prepare_request(req)
    resp = session.send(prepped)
    for past_resp in resp.history:
        print_response(past_resp)
        url = past_resp.url
        if 'SAMLRequest' not in url:
            continue
        parsed_url = urlparse.urlparse(url)
        parts = urlparse.parse_qs(parsed_url.query)
        saml_request = decode_saml_request(parts['SAMLRequest'][0])
        print_status('Got SAMLRequest', saml_request)
    print_response(resp)
    return resp


# Remove NotAfter

def get_saml_response(req, s):
    while True:
        # --- Send a response ---
        resp = run_request(req, s)

        form = ProcessForm(resp.text, resp.request.url)
        if not form.is_form:
            break

        payload = form.payload

        # FIXME
        try:
            for input_value in args.input:
                (k, v) = input_value.split('=')
                if k in payload:
                    payload[k] = v
        except:
            pass

        if 'username' in payload and args.username:
            payload['username'] = args.username
        if 'password' in payload and args.password:
            payload['password'] = args.password

        extra_msg = '"{method}" form to "{action}"'.format(
            method=form.method,
            action=form.action,
            )
        print_status(extra_msg, str(form))

        # Since we found a form, set up the next request
        req = Request(form.method, form.action, data=form.payload)
        # If there is a SAMLResponse in the form,
        # break out of this loop and connect to the SP
        if 'SAMLResponse' in form.payload:
            found_saml_response = True
            saml = decode_saml_response(form.payload['SAMLResponse'])
            print_status('Got SAMLResponse', saml)
            break
    return req

session = requests.Session()
first_request = Request('GET', args.idp_url)
req = get_saml_response(first_request, session)
s = session

found_saml_response = 'SAMLResponse' in req.data

if found_saml_response:
    print_status('Sending request to SP')
    run_request(req, s)
else:
    print_status('No SAMLResponse Found!', status=False)

if found_saml_response and args.mangle:
    from request_manglers import mangler_list
    for mangler_class in mangler_list:
        mangle_session = requests.Session()
        req = get_saml_response(first_request, mangle_session)

        print_status('Creating mangler: ' + str(mangler_class))
        mangler = mangler_class(req.data['SAMLResponse'])
        req.data['SAMLResponse'] = mangler.get()

        saml = decode_saml_response(req.data['SAMLResponse'])
        print_status('Mangled SAMLResponse Created', saml)

        # mangled_s = requests.Session()
        # resp = run_request(req, mangled_s)
        resp = requests.request(
            req.method,
            req.url,
            data=req.data,
            )
        (status_message, status) = mangler.success(resp)
        print_status(status_message, status=status)
        if args.debug:
            for past_resp in resp.history:
                print_response(past_resp)
            print_response(resp)
