Install:

$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt

Usage:

$ python messenger.py --help
$ python messenger.py --idp='http://localhost:7000/'
$ python messenger.py --idp='http://jfranusic.oktapreview.com/home/jfranusic_jftest20140728_1/0oa2l23qp9JEYPZMKUXA/6855' --username=saml-test --password=Password1 --mangle

Get your Okta URL as follows:

 * Log in to your Okta organization as an admin
 * Click the "Admin" button
 * Click on the "Applications" tab
 * Click on the application you want the URL for
 * Click on the "General" tab
 * Scroll to the bottom of the page, look for the "App Embed Link"
 * Copy the "App Embed Link" into your clipboard
 * Use the the App Embed Link as the value for the --idp flag

Example output:

$ python messenger.py --idp='http://localhost:7000/'
[ OK ] GET http://localhost:7000/ 200 OK
       "POST" form to "http://localhost:7000"
[ OK ] POST http://localhost:7000/ 200 OK
       "post" form to "http://localhost:5000/saml/sso/saml-idp"
       Got SAMLResponse
       Sending request to SP
[ OK ] POST http://localhost:5000/saml/sso/saml-idp 302 FOUND
[ OK ] GET http://localhost:5000/user 200 OK
