#
# saml-validate
#
# A SAML validation service
import os

from flask import Flask
from flask import request
from flask import jsonify
from flask import render_template

from validate import SAMLValidator
app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/api/v1/test', methods=['GET', 'POST'])
def api_test():
    rv = {
        "one": "Singular sensation",
        "two": "Beady little eyes",
        "three": "Little birds pitch by my doorstep"
    }
    return jsonify(**rv)


@app.route('/api/v1/session', methods=['POST'])
def session_start():
    form_fields = {}
    # FIXME: We should be sending JSON in the request.from POST
    # In particular, we want keys for 'form_fields' and 'options'
    for name in ['issuer', 'acs', 'audience', 'username', 'password',
                 'firstName', 'lastName']:
        if name in request.form:
            form_fields[name] = request.form[name]
    url = None
    username = None
    password = None
    if 'url' in request.form:
        url = request.form['url']
    if 'username' in form_fields:
        username = form_fields['username']
    if 'password' in form_fields:
        password = form_fields['password']

    rv = {
        'results': []
    }
    form_fields['login'] = form_fields['username']
    form_fields['email'] = form_fields['username']

    # FIXME: Move this to an environment variable
    # saml_idp = 'http://localhost:7000/'
    saml_idp = 'http://idp.oktadev.com/'
    try:
        validator = SAMLValidator(url=saml_idp,
                                  form_fields=form_fields,
                                  debug=True)
        if 'test_security' in request.form:
            test_security = (request.form['test_security'] == 'true')
            validator.test_security = test_security
        for result in validator.post_updates(form_fields):
            rv['results'].append(result)
    except Exception as e:
        result = {
            'status': 'FAIL',
            'summary': str(e),
        }
        rv['results'].append(result)

    return jsonify(**rv)


if __name__ == "__main__":
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    debug = False
    if port == 5000:
        debug = True
    app.run(host='0.0.0.0', port=port, debug=debug)
