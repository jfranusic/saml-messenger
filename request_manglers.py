#!/usr/bin/env python
import base64
from requests import Request
import requests
from lxml import etree
import zlib

from signxml import xmldsig
import signxml


class SAMLResponseMangler:
    def __init__(self, saml_response):
        decoded_saml_response = base64.b64decode(saml_response)
        self.fake_identifier = 'fake.identifier.example.com'
        self.tree = etree.fromstring(decoded_saml_response)
        self.cert = open("oktadev-idp-cert.pem").read()
        self.key = open("oktadev-idp-key.pem").read()
        self.ns = {
            'saml':   'urn:oasis:names:tc:SAML:2.0:assertion',
            'saml2':  'urn:oasis:names:tc:SAML:2.0:assertion',
            'samlp':  'urn:oasis:names:tc:SAML:2.0:protocol',
            'saml2p': 'urn:oasis:names:tc:SAML:2.0:protocol',
            'ds':     'http://www.w3.org/2000/09/xmldsig#',
            }

    def mangle(self):
        raise NotImplementedError

    def remove_signature(self):
        for e in self.tree.xpath('//ds:Signature', namespaces=self.ns):
            e.getparent().remove(e)

    def add_valid_signature(self):
        for k in self.ns.keys():
            etree.register_namespace(k, self.ns[k])
        tag = "{%s}Signature" % self.ns['ds']
        placeholder = etree.Element(tag, attrib={'Id': 'placeholder'})
        placeholder.text = ''
        self.tree.insert(1, placeholder)
        result = xmldsig(self.tree).sign(
            key=self.key,
            cert=self.cert,
        )

    def get(self):
        self.mangle()
        text = etree.tostring(self.tree)
        return base64.b64encode(text)

    def success(self, response):
        if 200 <= response.status_code < 400:
            return ("Oh no! The modified SAMLResponse was accepted",
                    False)
        else:
            return ("Modified SAMLResponse rejected", True)


class MangleReplaceDestination(SAMLResponseMangler):
    """replaces the contents of Destination tag"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//samlp:Response', namespaces=self.ns):
            e.attrib['Destination'] = self.fake_identifier
        self.add_valid_signature()


class MangleReplaceIssuer(SAMLResponseMangler):
    """replaces the contents of the saml2:Issuer tag"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml2:Issuer', namespaces=self.ns):
            e.text = self.fake_identifier
        self.add_valid_signature()


class MangleReplaceAudience(SAMLResponseMangler):
    """replaces the contents of the saml:Audience tag"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml:Audience', namespaces=self.ns):
            e.text = self.fake_identifier
        self.add_valid_signature()


class MangleRemoveIssuer(SAMLResponseMangler):
    """removes the saml2:Issuer tag"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml2:Issuer', namespaces=self.ns):
            e.getparent().remove(e)
        self.add_valid_signature()


class MangleRemoveSignature(SAMLResponseMangler):
    """removes all ds:Signature tags"""
    def mangle(self):
        self.remove_signature()


class MangleSwapSignature(SAMLResponseMangler):
    """re-sign assertion with the idp.oktadev.com key"""
    def mangle(self):
        self.remove_signature()
        self.add_valid_signature()

    def success(self, response):
        if 200 <= response.status_code < 400:
            return ("The SP is working as expected", True)
        else:
            return ("Unexpected issue SP issue validating signed SAML", False)


class MangleReplaceSignatureWithUnknown(SAMLResponseMangler):
    """replace signature with one from a valid, but unknown, key"""
    def mangle(self):
        self.remove_signature()
        self.cert = open("example-idp-cert.pem").read()
        self.key = open("example-idp-key.pem").read()
        self.add_valid_signature()


class MangleRemoveSignatureAndAssertionID(SAMLResponseMangler):
    """
    removes signatures and replaces saml:NameID and saml:Assertion contents
    """
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml:NameID', namespaces=self.ns):
            e.text = 'admin@example.com'
        for e in self.tree.xpath('//saml:Assertion', namespaces=self.ns):
            del(e.attrib['ID'])


class MangleRemoveConfirmationDataNotOnOrAfter(SAMLResponseMangler):
    """removes NotOnOrAfter from saml:SubjectConfirmationData"""
    def mangle(self):
        self.remove_signature()
        #           SubjectConfirmationData
        tag = '//saml:SubjectConfirmationData'
        for e in self.tree.xpath(tag, namespaces=self.ns):
            del(e.attrib['NotOnOrAfter'])
        self.add_valid_signature()


class MangleRemoveNotOnOrAfter(SAMLResponseMangler):
    """removes NotOnOrAfter from saml:Conditions"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml:Conditions', namespaces=self.ns):
            del(e.attrib['NotOnOrAfter'])
        self.add_valid_signature()


class MangleRemoveTimeConditions(SAMLResponseMangler):
    """removes NotBefore and NotOnOrAfter from saml:Conditions"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml:Conditions', namespaces=self.ns):
            del(e.attrib['NotBefore'])
            del(e.attrib['NotOnOrAfter'])
        self.add_valid_signature()


class MangleMakeStaleAssertion(SAMLResponseMangler):
    """set NotBefore and NotOnOrAfter to old dates"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//saml:Conditions', namespaces=self.ns):
            e.attrib['NotBefore'] = "1997-09-29T06:14:00.000Z"
            e.attrib['NotOnOrAfter'] = "2004-07-25T22:18:00.000Z"
        self.add_valid_signature()


class MangleReplaceNameID(SAMLResponseMangler):
    """replaces the conents of saml:NameID"""
    def mangle(self):
        for e in self.tree.xpath('//saml:NameID', namespaces=self.ns):
            e.text = 'admin@example.com'


class MangleStatusCode(SAMLResponseMangler):
    """changes the contents of samlp:StatusCode to 'Failure'"""
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//samlp:StatusCode', namespaces=self.ns):
            e.attrib['Value'] = 'Failure'
        self.add_valid_signature()


class MangleStatusCodeRequester(SAMLResponseMangler):
    """
    changes the conents of samlp:StatusCode to
    'urn:oasis:names:tc:SAML:2.0:status:Requester'
    """
    def mangle(self):
        self.remove_signature()
        for e in self.tree.xpath('//samlp:StatusCode', namespaces=self.ns):
            e.attrib['Value'] = 'urn:oasis:names:tc:SAML:2.0:status:Requester'
        self.add_valid_signature()


class MangleCompressRequest(SAMLResponseMangler):
    """compresses the SAML Request"""
    def get(self):
        text = etree.tostring(self.tree)
        compressed = zlib.compress(text)
        # Strip the first 2 bytes (header)
        # and the last 4 bytes (checksum) to get the raw deflate
        deflated = compressed[2:-4]
        return base64.b64encode(deflated)

    def success(self, response):
        if 200 <= response.status_code < 400:
            return ("The SP supports a compressed SAMLResponse", True)
        else:
            return ("The SP does not support a compressed SAMLResponse", False)


mangler_list = [
    MangleRemoveSignature,
    MangleSwapSignature,
    MangleReplaceSignatureWithUnknown,
    MangleReplaceDestination,
    MangleReplaceIssuer,
    MangleReplaceAudience,
    MangleRemoveIssuer,
    MangleRemoveConfirmationDataNotOnOrAfter,
    MangleRemoveNotOnOrAfter,
    MangleRemoveTimeConditions,
    MangleMakeStaleAssertion,
    MangleRemoveSignatureAndAssertionID,
    MangleReplaceNameID,
    MangleStatusCode,
    MangleStatusCodeRequester,
    MangleCompressRequest,
    ]
